Tegan Barry - Atlassian Project

Built with

------

JS

AngularJS

I run it using "HTTP Server" 

------

CSS

Built using Bootstrap

BEM naming convention for class names

------

Testing

Tests built using Karma and Jasmine

To run the tests you will first have to install karma: http://karma-runner.github.io/1.0/intro/installation.html

Run it using 'karma start' from the project directory.  The results should be:

Executed 0 of 6 SUCCESS

Executed 1 of 6 SUCCESS

Executed 2 of 6 SUCCESS

Executed 3 of 6 SUCCESS

Executed 4 of 6 SUCCESS

Executed 5 of 6 SUCCESS

Executed 6 of 6 SUCCESS

Executed 6 of 6 SUCCESS

------
