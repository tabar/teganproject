(function() {
	'use strict';
		var app = angular.module("atlassianApp", []);

		app.controller("articlesCtrl", ["$scope", "$http", function($scope, $http){
		
		$scope.items = {};

		$scope.titles = {};

		$scope.activeTitle;
		$scope.activeArticle;

		$http ({
			method: 'GET',
			url: "js/sessions.json"
		}).then(function successCallback(data, status, headers, config) {
			$scope.items = data.data;

			// Pulls out names of tab titles
			for (var i = data.data.Items.length - 1; i >= 0; i--) {
				$scope.titles[data.data.Items[i].Track.Title] = data.data.Items[i].Track.Title;
			}

			//find first track and first article within that track to use as defaults.
			var firstTrack = $scope.titles[Object.keys($scope.titles)[0]]; //get first track from list of tracks
			var firstArticle; //will be used to hold first article data
			//loop through each item and get first article that matches track
			angular.forEach($scope.items.Items, function(value, key) {
			//if previously set ignore, angular does not support 'break;'
				if (!firstArticle && (value.Track.Title == firstTrack)) {
					firstArticle = value;
				}
			});

			$scope.showArticles(firstTrack); //preload first track as default
			$scope.articleShow(firstArticle); //preload first article within that track as default
		})

		$scope.showArticles = function (title) {
			$scope.current = title;
			$scope.sameTitle = [];
			$scope.activeTitle = title;

			var firstArticle; //will be used to hold first article data
			angular.forEach($scope.items.Items, function(value, key) {
				if (value.Track.Title == title) {
					$scope.sameTitle.push(value);
					//if previously set ignore, angular does not support 'break;'
					if (!firstArticle) {
						firstArticle = value;
					}
				}
			})
			$scope.articleShow(firstArticle); //preload first article within this track as default
		}

		$scope.articleShow = function (item) {
			$scope.activeArticle = item;
			$scope.article = item;
		}	
	}]);
})();